# -*- coding: utf-8 -*-
import logging

logging.basicConfig(level=logging.INFO)

BOT_MODES = ("polling", "webhook")
BOT_MODE = BOT_MODES[0]
BOT_TOKEN = "Please, set token in settings_local.py"
CAT_API = "http://thecatapi.com/api/images/get?format=html"

try:
    from .settings_local import *
except ImportError:
    logging.info("No local settings.")
