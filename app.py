# -*- coding: utf-8 -*-
import logging
import requests
from datetime import datetime
from bs4 import BeautifulSoup
from telegram import Bot
from telegram.ext import Updater
from telegram.ext import Filters
from telegram.ext import CommandHandler, MessageHandler
from settings import settings


class Application(object):
    def __init__(self):
        self.updater = Updater(token=settings.BOT_TOKEN)
        self.bot = Bot(token=settings.BOT_TOKEN)

    def __start_polling(self):
        self.updater.dispatcher.add_handler(CommandHandler('start', self.cmd_start))
        self.updater.dispatcher.add_handler(MessageHandler(Filters.text, self.cmd_uknown))
        logging.info("Bot started with polling mode.")
        self.updater.start_polling()

    # noinspection PyMethodMayBeStatic
    def __start_webhook(self):
        raise NotImplemented()

    def __send_msg(self, chat_id, text, bot=None):
        if bot is None:
            bot = self.bot
        try:
            bot.sendMessage(chat_id=chat_id, text=text)
        except Exception as e:
            logging.error("An error occurred at %s: %s" % (datetime.now(), e))

    def start(self):
        if settings.BOT_MODE == "polling":
            self.__start_polling()
        elif settings.BOT_MODE == "webhook":
            self.__start_webhook()
        else:
            raise NotImplementedError("BOT_MODE should be 'polling' or 'webhook'!")

    def cmd_start(self, bot, update):
        logging.info("new user at %s: chat_id %s" % (datetime.now(), update.message.chat_id))
        self.__send_msg(update.message.chat_id, self.get_kitty(), bot=bot)

    def cmd_uknown(self, bot, update):
        logging.info("receive msg from chat_id %s at %s: %s" % (
            update.message.chat_id, datetime.now(), update.message.text
        ))
        self.__send_msg(update.message.chat_id, self.get_kitty(), bot=bot)

    @staticmethod
    def get_kitty():
        kitty = requests.get(settings.CAT_API)
        soup = BeautifulSoup(kitty.text, 'html.parser')
        return soup.img["src"]


def main():
    app = Application()
    app.start()


if __name__ == "__main__":
    main()
